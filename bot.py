import os
import json
from schema import Schema, Regex
from docopt import docopt
from nio import AsyncClient
from typing import Dict, List


class Bookmark(object):
    def __init__(self, name: str, mxc_uri: str) -> None:
        """
        A map referencing a Matrix Content (MXC) URI to a given name.
        """
        self._name = name
        self._mxc_uri = mxc_uri

    def __repr__(self):
        return f"{self._name} -> {self._mxc_uri}"

    @property
    def name(self) -> str:
        """
        The name given to the bookmark.
        """
        return self._name

    @property
    def mxc_uri(self) -> str:
        """
        The Matrix Content (MXC) URI which has the given format:
        mxc://<server-name>/<bookmark-id>
        """
        return self._mxc_uri

    def validate(self) -> None:
        print(self._mxc_uri)
        uri_pattern = r"mxc://.*/\w{24}"
        Regex(
            uri_pattern,
            error=f"The mxc uri:{self._mxc_uri} is not proplery formatted. It should be in the given form: (i.e: mxc://<server-name>/<bookmark-id>)",
        ).validate(self._mxc_uri)
        Schema(str).validate(self._name)


class BookmarkRepository:
    def __init__(self, json_path: str) -> None:
        """ The place containing all the saved bookmarks for a specific room.
        """
        self._json_path = json_path
        if os.path.exists(json_path):
            self._load()
        else:
            self._bookmarks: List[Bookmark] = []

    def get(self, name: str) -> Bookmark:
        return next(bookmark for bookmark in self._bookmarks if bookmark.name == name)

    def add(self, bookmark: Bookmark) -> None:
        bookmark.validate()
        existing_names = [bookmark.name for bookmark in self._bookmarks]
        if bookmark.name in existing_names:
            raise ValueError(f"A bookmark named {bookmark.name} already exists.")
        self._bookmarks.append(bookmark)
        self._dump()

    def remove(self, name: str) -> None:
        bookmark = self.get(name)
        self._bookmarks.remove(bookmark)

    def list(self) -> List[Bookmark]:
        return self._bookmarks

    def _load(self) -> None:
        with open(self._json_path) as f:
            self._bookmarks = json.load(f, object_hook=self._deserialize)

    def _deserialize(self, dct):
        return Bookmark(dct["name"], dct["mxc"])

    def _dump(self):
        with open(self._json_path, "w") as f:
            json.dump(
                self._bookmarks, f, cls=self.ComplexEncoder, sort_keys=True, indent=4
            )

    class ComplexEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, Bookmark):
                return {"mxc": obj.mxc_uri, "name": obj.name}
            # Let the base class default method raise the TypeError
            return json.JSONEncoder.default(self, obj)


class BookmarkBot(object):
    """ A bot that bookmarks media to make it easy to share them back quickly.
    Usage:
    !bm <name>
    !bm add <mxcuri> <name>
    !bm remove <mxcuri> <name>
    !bm (-l | --list)
    !bm (-h | --help)

    Arguments:
    <name>    name under which the media is stored
    <mxcuri>  The Matrix Content (MXC) URI which has the given format: mxc://<server-name>/<media-id>

    Options:
    -l --list     List all available bookmarks.
    -h --help     Show this screen.
    """

    def __init__(self, nio_client: AsyncClient) -> None:
        self._nio_client = nio_client
        self._repositories: Dict[str, BookmarkRepository] = {}

    async def parse(self, args: List[str], room_id: str) -> None:
        try:
            arguments = docopt(self.__doc__, args)
            repo = self._get_or_create_repository(room_id)
            if arguments["add"]:
                self._add(room_id, repo, arguments["<name>"], arguments["<mxcuri>"])
            elif arguments["remove"]:
                self._remove(room_id, repo, arguments["<name>"])
            elif arguments["--list"]:
                await self._list(room_id, repo)
            elif arguments["--help"]:
                await self._send_message(room_id, self.__doc__)
            else:
                await self._send(room_id, repo, arguments["<name>"])
        except SystemExit:
            await self._send_message(room_id, self.__doc__)
        except Exception as e:
            print(str(e))
            await self._send_message(room_id, f"Syntax error: !bm -h to see usage")

    def _get_or_create_repository(self, room_id: str) -> BookmarkRepository:
        repo = self._repositories.get(room_id)
        return repo if repo is not None else self._create_repository(room_id)

    def _create_repository(self, room_id: str) -> BookmarkRepository:
        current_path = os.path.dirname(os.path.realpath(__file__))
        bookmarks_path = os.path.join(current_path, f"bookmarks_{room_id}.json")
        repo = BookmarkRepository(bookmarks_path)
        self._repositories[room_id] = repo
        return repo

    def _add(
        self, room_id: str, repo: BookmarkRepository, name: str, mxc_uri: str
    ) -> None:
        """Bookmark a media with the given name."""
        bookmark = Bookmark(name, mxc_uri)
        repo.add(bookmark)

    def _remove(self, room_id: str, repo: BookmarkRepository, name: str) -> None:
        """Remove a bookmark from its name."""
        repo.remove(name)

    async def _list(self, room_id: str, repo: BookmarkRepository) -> None:
        bookmarks = repo.list()
        await self._send_message(room_id, str(bookmarks))

    async def _send(self, room_id: str, repo: BookmarkRepository, name: str) -> None:
        """Send the given bookmark."""
        bookmark = repo.get(name)
        if bookmark is None:
            self._send_message(
                f"Bookmark named {name} does not exist. Please run '!bm --list' to see all existing bookmarks in this room."
            )
        content = {
            "url": bookmark.mxc_uri,
            "msgtype": "m.image",
            "body": "img.jpg",
            "info": {},
        }
        await self._nio_client.room_send(
            room_id, "m.room.message", content, ignore_unverified_devices=True
        )

    async def _send_message(self, room_id, message):
        content = {"body": message, "msgtype": "m.notice"}
        await self._nio_client.room_send(
            room_id, "m.room.message", content, ignore_unverified_devices=True
        )
