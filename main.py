import asyncio
import os
import configparser
import sys
import logbook
import nio
from logbook import Logger, StreamHandler
from nio import (
    AsyncClient,
    ClientConfig,
    MegolmEvent,
    RoomMessageText,
    SyncResponse,
    RoomKeyRequest,
    InviteMemberEvent,
    Event,
    RoomKeyEvent,
)
from bot import BookmarkBot

# megolm = list()


async def message_cb(room, event):
    if not event.body.startswith("!bm"):
        return
    args = event.body.split()
    args.pop(0)
    await bmbot.parse(args, room.machine_name)


async def sync_cb(response: SyncResponse):
    """ A callback that will be called every time our `sync_forever`
    method succesfully syncs with the server.
    """
    with open("next_batch", "w") as next_batch_token:
        next_batch_token.write(response.next_batch)


def key_share_cb(event: RoomKeyRequest):
    user_id = event.sender
    device_id = event.requesting_device_id
    device = client.device_store[user_id][device_id]
    client.verify_device(device)
    for request in client.get_active_key_requests(user_id, device_id):
        client.continue_key_share(request)


async def call_cb(room, event):
    await client.join(room.machine_name)


async def invited_cb(room, event):
    await client.join(room.room_id)


# TODO: Implement this callback for MegolmEvent
# async def megolm_cb(room, event):
#     megolm.append(event)

# async def room_key_cb(event):
# if event is MegolmEvent:
#     for m in megolm:
#         if event.session_id == m.session_id:
#             decrypt = client.decrypt_event(event)


async def main():
    client.add_event_callback(invited_cb, InviteMemberEvent)
    client.add_event_callback(message_cb, RoomMessageText)
    client.add_event_callback(invited_cb, InviteMemberEvent)
    client.add_response_callback(sync_cb, SyncResponse)
    client.add_to_device_callback(key_share_cb, RoomKeyRequest)
    # client.add_to_device_callback(room_key_cb, RoomKeyEvent)
    # client.add_event_callback(megolm_cb, MegolmEvent)

    password = config.get("user", "password")
    await client.login(password, "DEVICEBOT")
    with open("next_batch", "r+") as next_batch_token:
        client.next_batch = next_batch_token.read()
    await client.sync_forever(timeout=30000, full_state=True)


if __name__ == "__main__":
    cfg_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "bot.cfg")
    if not os.path.isfile(cfg_file):
        print(f"config file {cfg_file} not found")
        sys.exit(1)
    config = configparser.ConfigParser()
    config.read(cfg_file)
    StreamHandler(sys.stdout).push_application()
    log = Logger("Logbook")
    nio.crypto.logger.level = logbook.ERROR
    host = config.get("user", "host")
    user = config.get("user", "user")
    device_id = config.get("user", "device_id")
    client_config = ClientConfig(encryption_enabled=True)
    client = AsyncClient(host, user, store_path=".", config=client_config)
    bmbot = BookmarkBot(client)
    asyncio.get_event_loop().run_until_complete(main())
