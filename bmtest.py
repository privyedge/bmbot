import pytest
from repository import BookmarkRepository, Bookmark
from schema import SchemaError


def media_json():
    media_json = """[
    {
        "mxc": "mxc://server.org/IFmNVCjYOAPGGgBjmtJywfdN",
        "name": "test1"
    },
    {
        "mxc": "mxc://server.org/ZqxsxLqpKcjAZgSXgEBuKZGL",
        "name": "test2"
    },
    {
        "mxc": "mxc://server.org/LmFbXvVIgcuSzpvrmHBMysDQ",
        "name": "test3"
    }
]"""
    return media_json


@pytest.fixture
def create_repo(tmp_path):
    json_test = tmp_path / "media_test.json"
    json_test.write_text(media_json())
    return json_test


def test_json_dump(tmp_path):
    json_test = tmp_path / "test.json"
    repo = BookmarkRepository(json_test)
    media = Bookmark("test1", "mxc://server.org/IFmNVCjYOAPGGgBjmtJywfdN")
    media2 = Bookmark("test2", "mxc://server.org/ZqxsxLqpKcjAZgSXgEBuKZGL")
    media3 = Bookmark("test3", "mxc://server.org/LmFbXvVIgcuSzpvrmHBMysDQ")
    repo.add(media)
    repo.add(media2)
    repo.add(media3)
    assert json_test.read_text() == media_json()


def test_add_existing_name(create_repo):
    repo = BookmarkRepository(create_repo)
    media = Bookmark("test1", "mxc://server.org/LmFbXvVIgcuSzpvrmHBMysDQ")
    with pytest.raises(ValueError):
        repo.add(media)


def test_wrong_mxc():
    with pytest.raises(SchemaError):
        bookmark = Bookmark("test", "mxc://something.org/12ZER")
        bookmark.validate()


def test_get_bookmark(create_repo):
    repo = BookmarkRepository(create_repo)
    bookmark = repo.get("test1")
    assert bookmark.name == "test1"
    assert bookmark.mxc_uri == "mxc://server.org/IFmNVCjYOAPGGgBjmtJywfdN"


def test_remove_bookmark(create_repo):
    repo = BookmarkRepository(create_repo)
    repo.remove("test1")
    assert len(repo.list()) == 2
